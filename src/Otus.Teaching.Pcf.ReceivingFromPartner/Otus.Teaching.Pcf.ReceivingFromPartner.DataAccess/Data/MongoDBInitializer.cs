﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Data
{
    public class MongoDBInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Partner> partners;
        private readonly IMongoCollection<Preference> preferences;

        public MongoDBInitializer(
            IMongoCollection<Partner> partners,
            IMongoCollection<Preference> preferences)
        {
            this.partners = partners;
            this.preferences = preferences;
        }

        private async void DeleteCreateInitializeCollection<T>(
            IMongoCollection<T> collection, IEnumerable<T> initializeData)
        {
            await collection.DeleteManyAsync(_ => true);
            await collection.InsertManyAsync(initializeData);
        }
        public void InitializeDb()
        {
            DeleteCreateInitializeCollection(partners, FakeDataFactory.Partners);
            DeleteCreateInitializeCollection(preferences, FakeDataFactory.Preferences);
        }
    }

}
