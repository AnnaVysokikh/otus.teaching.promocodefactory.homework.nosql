﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models
{
    public class MongoDbConfigOptions
    {
        public string Connection { get; set; }
        public string DatabaseName { get; set; }
        public string PartnerCollectionName { get; set; }
        public string PartnerPromoCodeLimitCollectionName { get; set; }
        public string PreferenceCollectionName { get; set; }
        public string PromoCodeCollectionName { get; set; }

    }
}