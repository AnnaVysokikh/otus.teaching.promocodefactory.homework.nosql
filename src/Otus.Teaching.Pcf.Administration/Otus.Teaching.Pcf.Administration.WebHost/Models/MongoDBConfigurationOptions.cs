﻿namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class MongoDBConfigurationOptions
    {
        public string Connection { get; set; }
        public string DatabaseName { get; set; }
        public string EmployeeCollectionName { get; set; }
        public string RoleCollectionName { get; set; }
    }
}
